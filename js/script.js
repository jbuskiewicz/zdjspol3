// STEP 4 - Object.create() jako sposob na wspodzielenie metod bez koniecznosci ich ponownej implementacji w funkcji Animal
// problem? metody "klasy" Animal nadal są dostępne w formie zewnętrznego (dodatkowego) obiektu

// const animalMethods = {
//   sleep: function (sleepLength) {
//     // console.log - Reksio spi..
//     console.log(`${this.name} spi...`);
//     // dodajemy wartosc do pola energy
//     this.energy += sleepLength;
//   },

//   play: function (time) {
//     console.log(`${this.name} sie bawi..`);
//     this.energy -= time;
//   },
//   eat() {
//     console.log(`${this.name} wcina obiadek`);
//   },
// };

// function Animal(name, type, energy) {
//   let animal = Object.create(animalMethods); // tworzymy pusty obiekt {}, ale mowimy JS, ze jezeli pole ktorego bedzie szukal nie zostanie utworzone w tym obiekcie - szukaj tego pola w obiekcie podanym jako parametr metody statycznej create

//   animal.name = name;
//   animal.type = type;
//   animal.energy = energy;

//   return animal;
// }

// const reksio = Animal("Reksio", "Dog", 10);
// const filemon = Animal("Filemon", "Cat", 20);

// console.log(reksio);
// reksio.sleep(5);

// JS do kazdego tworzonego obiektu dodaje wlasciwosc "prototype" ktora jest obiektem
// w przeglądarce odnosimy się do prototypu obiektu przez pole "__proto__" => "dunder proto" => "double underline"
// [[Prototype]]

// ----------------------------------------------------------------------------------------------------

// Object.create - jak działa

// Parent - child

const parent = {
  name: "Jan",
  age: 40,
  nationality: "Polish",
};

// const child = {
//   name: "Tomek",
//   age: 8,
//   nationality: "Polish"
// };

const child = Object.create(parent);
child.name = "Tomek";
child.age = 8;

// console.log(child);
// console.log(child.nationality);

// console.log(parent.hasOwnProperty("nationality"));
// console.log(child.hasOwnProperty("nationality"));

// ----------------------------------------------------------------------------------------------------

// STEP 5 - prototype

// function Animal(name, type, energy) {
//   let animal = Object.create(Animal.prototype); // tworzymy pusty obiekt {}, ale mowimy JS, ze jezeli pole ktorego bedzie szukal nie zostanie utworzone w tym obiekcie - szukaj tego pola w obiekcie podanym jako parametr metody statycznej create

//   animal.name = name;
//   animal.type = type;
//   animal.energy = energy;

//   return animal;
// }

// console.log(Animal.prototype);

// Animal.prototype.sleep = function (sleepLength) {
//   // console.log - Reksio spi..
//   console.log(`${this.name} spi...`);
//   this.energy += sleepLength;
// };

// Animal.prototype.play = function (time) {
//   console.log(`${this.name} sie bawi..`);
//   this.energy -= time;
// };

// const reksio = Animal("Reksio", "Dog", 10);
// console.log(reksio);

// STEP 6 - new, dalsze upraszczanie

// function Animal(name, type, energy) {
//   // let animal = Object.create(Animal.prototype); // tworzymy pusty obiekt {}, ale mowimy JS, ze jezeli pole ktorego bedzie szukal nie zostanie utworzone w tym obiekcie - szukaj tego pola w obiekcie podanym jako parametr metody statycznej create

//   this.name = name;
//   this.type = type;
//   this.energy = energy;

//   // return animal;
// }

// const reksio = new Animal("Reksio", "Dog", 10);
// console.log(reksio);

// class Animal2 {
//   constructor(name, type, energy) {
//     this.name = name;
//     this.type = type;
//     this.energy = energy;
//   }

//   sleep(sleepLength) {
//     // console.log - Reksio spi..
//     console.log(`${this.name} spi...`);
//     this.energy += sleepLength;
//   }

//   play(time) {
//     console.log(`${this.name} sie bawi..`);
//     this.energy -= time;
//   }
// }

// prototype chain
// console.log(reksio instanceof Object);
// console.log(Array.prototype.__proto__);

// ------------------------------------------------------------------------

// Dziedziczenie - 1 paradygmat OOP

// Stworzmy klase Animal, ktora bedzie zawierala 3 pola (name, breed, energy) oraz 3 metody (eat, sleep, poop)

class Animal {
  constructor(name, breed, energy) {
    this.name = name;
    this.breed = breed;
    this.energy = energy;
  }

  eat(food) {
    console.log(`${this.name} je`);
    this.energy += food;
  }
  sleep(sleepLength) {
    if (!sleepLength) return;
    console.log(`${this.name} śpi`);
    this.energy += sleepLength;
  }
  poop() {
    this.energy -= 2;
  }
}

// klasa Animal będzie pełniła rolę klasy bazowej do tworzenia klas potomnych

class Dog extends Animal {
  constructor(name, breed, energy) {
    super(name, breed, energy); // wywowalnie konstruktora klasy bazowej, ktory zwroci odpowiednie pola i metody do klasy potomnej
  }

  bark() {
    console.log("woof woof!");
  }
}

const reksio = new Dog("Reksio", "kundelek", 50);
// reksio.bark();
// console.log(reksio);

// Stworz dwie klasy potomne bazujace na klasie bazowej Animal np. Cat i Bird. Dodaj do kazdej z tych klas metode wlasciowa dla danej klasy (np. drinkMilk oraz fly)

class Cat extends Animal {
  constructor(name, breed, energy) {
    super(name, breed, energy); // wywowalnie konstruktora klasy bazowej, ktory zwroci odpowiednie pola i metody do klasy potomnej
  }
  drinkMilk() {
    console.log(`${this.name} drinking milk`);
    this.energy += 2;
  }
}
const kot = new Cat("Malik", "British", 10);
// kot.drinkMilk();
// console.log(kot);

class Bird extends Animal {
  constructor(name, breed, energy) {
    super(name, breed, energy);
  }
  fly() {
    console.log(`${this.name} leci`);
    this.energy -= 2;
  }
}
const ptak = new Bird("Marika", "Sroka", 100);
// ptak.fly();
// console.log(ptak);

// Abstrakcja - 2 paradygmat OOP

class Person {
  constructor(firstName, lastName, job) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.job = job;
    this.skills = [];
    Person._amount = Person._amount || 0;
    // if (Person._amount) {
    //   return Person._amount
    // } else {
    //   return 0;
    // }
    // ternary operator / elvis operator
    // return Person._amount >= 0 ? Person._amount : 0;
    Person._amount++;
  }

  static get amount() {
    return Person._amount;
  }

  learn(skill) {
    this.skills.push(skill);
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  set fullName(fullName) {
    // metoda .split() zwraca tablice, splitterem jest w tym przypadku spacja
    // przy pomocy mechanizmu destrukturyzacji z indeksu 0 pobieramy imie, a z indeksu 1 nazwisko
    [this.firstName, this.lastName] = fullName.split(" ");
  }
}

class Job {
  constructor(company, position, salary) {
    this.company = company;
    this.position = position;
    this.salary = salary;
  }
}

const john = new Person("John", "Doe", new Job("Google", "Engineer", 10000));
const john2 = new Person("John", "Doe", new Job("Google", "Engineer", 10000));
const john3 = new Person("John", "Doe", new Job("Google", "Engineer", 10000));
const john4 = new Person("John", "Doe", new Job("Google", "Engineer", 10000));

// do klasy Person dodaj pole "skills" ktore bedzie tablica umiejetnosci
// do klasy Person dodaj metode "learn" ktora przyjmie jako parametr "skill" i doda go do tablicy "skills"

john.learn("js");
john.learn("typescript");
john.learn("es6");
// console.log(john);

// fetch("https://jsonplaceholder.typicode.com/users")
//   .then((response) => response.json())
//   .then((json) => console.log(json));

// ------------------------------------------------------------------------------------------------------------------------

// Cwiczenie
// Stworz aplikacje, ktora bedzie pobierala dane uzytkownika z API, a nastepnie zapisze je w stanie aplikacji

// Stan aplikacji - obiekt stworzony na bazie klasy, posiada metody: setData, addUser, clearData i pola currentUserId = 1, listOfUsers = []

// Kazdy pobrany user bedzie instancja klasy User, mozemy wykorzystac geter i seter do zarzadzania imieniem i nazwiskiem, a do adresu sam geter
// W dokumencie HTML stworz button, ktory po kliku pobierze dane usera (event)
// Po kazdym kliku zwiekszamy ID aktualnego usera, pobieramy dane z API, na podstawie zwrotki tworzymy nowa instancje klasy User a nastepnie dodajemy ja do stanu aplikacji
// Po dodaniu encji do stanu aplikacji - aktualizujemy dokument HTML, aby pobrany uzytkownik pojawil sie na liscie

// Bananas and gorillas problem

class AppState {
  constructor() {
    this.userId = 1;
    this.listOfUsers = [];
  }
  setData(newListOfUsers) {
    if (!newListOfUsers.length) throw Error("Array cannot be empty!");
    this.listOfUsers = [...newListOfUsers];
  }
  addUser(user) {
    // ! - znak negacji. !0 => 1, !1 => 0
    if (!user.id) throw Error("User property is empty!");
    this.userId += 1;
    this.listOfUsers.push(user);
  }
  clearData() {
    this.userId = 1;
    this.listOfUsers = [];
  }
}

class User {
  constructor(id, name, username, email, address, company) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
    this.address = address;
    this.company = company;
  }
  get fullName() {
    return `${this.name}, ${this.username}`;
  }
  set fullName(fullName) {
    [this.name, this.username] = fullName.split(" ");
  }
  get fullAddress() {
    return `${this.address.street}`;
  }
}

const appState = new AppState();

// pobrac button z HTML do JS
// dodac event
// odpalic pobieranie na klik

const getUserDataButton = document.getElementById("getUserDataButton");
const userList = document.getElementById("usersList");
const currentUserId = document.getElementById("currentUserId");

const onGetUserDataButtonClick = () => {
  fetch(`https://jsonplaceholder.typicode.com/users/${appState.userId}`)
    .then((response) => response.json())
    .then((data) => {
      const { id, name, username, email, address, company } = data;
      const user = new User(id, name, username, email, address, company);
      // dodajemy usera do stanu
      appState.addUser(user);
      
      // aktualizujemy HTML
      currentUserIdUpdate(id);
      addUserToHtml(user, userList);
    });
};

const addUserToHtml = (user, userListHandler) => {
  const { id, name, username } = user;
  const userElement = document.createElement("li");
  userElement.innerHTML = `<p>${id} - ${name} - ${username}</p>`;
  userListHandler.appendChild(userElement);
};

function currentUserIdUpdate(userId) {
  currentUserId.innerHTML = userId;
}

getUserDataButton.addEventListener("click", onGetUserDataButtonClick);
/**
 * 
<ul id="usersList"></ul>
<button id="getUserDataButton">Get user data</button>
 * 
 {
  "id": 1,
  "name": "Leanne Graham",
  "username": "Bret",
  "email": "Sincere@april.biz",
  "address": {
    "street": "Kulas Light",
  },
  "company": {
    "name": "Romaguera-Crona",
  }
}
 */
